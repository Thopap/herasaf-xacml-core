<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>org.herasaf.xacml.core</groupId>
	<artifactId>herasaf-xacml-core</artifactId>
	<packaging>bundle</packaging>
	<version>1.0.4.RELEASE</version>

	<name>HERAS-AF XACML :: Core</name>
	<description>The core component contains the evaluation engine. This means: combining algorithms, functions, data types, and API interfaces.</description> <!-- mandatory for HERAS-AF -->
	<url>https://bitbucket.org/herasaf/herasaf-xacml-core</url>

	<inceptionYear>2006</inceptionYear>
	<licenses>
		<license>
			<name>Apache License Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0</url>
			<distribution>repo</distribution>
		</license>
	</licenses>
	<organization>
		<name>HERAS-AF</name>
		<url>https://bitbucket.org/herasaf/herasaf-xacml-core</url>
	</organization>
	<developers>
		<developer>
			<name>Florian Huonder</name>
			<organization>HERAS-AF</organization>
			<organizationUrl>https://bitbucket.org/herasaf/herasaf-xacml-core</organizationUrl>
		</developer>
	</developers>

	<issueManagement>
		<system>JIRA</system>
		<url>https://herasaf.jira.com/projects/XACMLCORE</url>
	</issueManagement>
	<ciManagement>
		<system>Atlassian Bamboo</system>
		<url>https://herasaf.jira.com/builds</url>
	</ciManagement>
	<scm>
		<connection>
			scm:git:ssh://git@bitbucket.org:herasaf/herasaf-xacml-core.git
		</connection>
		<developerConnection>
			scm:git:ssh://git@bitbucket.org:herasaf/herasaf-xacml-core.git
		</developerConnection>
		<url>
			https://bitbucket.org/herasaf/herasaf-xacml-core
		</url>
		<tag>HEAD</tag>
	</scm>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<java.compile.version>1.5</java.compile.version>

		<reporting.findbugs.excludeFile.location>src/main/config/reporting/findbugs-excludes.xml</reporting.findbugs.excludeFile.location>
		<reporting.checkstyle.excludeFile.location>src/main/config/reporting/checkstyle-suppressions.xml</reporting.checkstyle.excludeFile.location>

		<!-- Dependencies versions -->
		<jaxb.version>2.2.4-1</jaxb.version> <!-- attention: this is the latest version that supports Java 5 -->
		<jodatime.version>2.9.1</jodatime.version>
		<slf4j.version>1.7.13</slf4j.version>

		<testng.version>6.8.21</testng.version> <!-- attention: this is the latest version that supports Java 5 -->
		<junit.version>4.12</junit.version>
		<xmlunit.version>1.6</xmlunit.version>
		<logback.version>1.0.13</logback.version>  <!-- attention: this is the latest version that supports Java 5 -->

		<bundle.plugin.version>2.4.0</bundle.plugin.version> <!-- attention: this is the latest version that supports Java 5 -->
		<compiler.plugin.version>3.3</compiler.plugin.version>
		<javadoc.plugin.version>2.10.3</javadoc.plugin.version>
		<source.plugin.version>2.4</source.plugin.version>
		<surefire.plugin.version>2.19</surefire.plugin.version>
		<release.plugin.version>2.4.2</release.plugin.version> <!-- attention: this is the latest version that supports Java 5 -->
	</properties>

	<dependencies>
		<dependency>
			<groupId>com.sun.xml.bind</groupId>
			<artifactId>jaxb-impl</artifactId>
			<version>${jaxb.version}</version>
		</dependency>
		<dependency>
			<groupId>joda-time</groupId>
			<artifactId>joda-time</artifactId>
			<version>${jodatime.version}</version>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>${slf4j.version}</version>
		</dependency>
		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-classic</artifactId>
			<version>${logback.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.testng</groupId>
			<artifactId>testng</artifactId>
			<version>${testng.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>${junit.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>xmlunit</groupId>
			<artifactId>xmlunit</artifactId>
			<version>${xmlunit.version}</version>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.felix</groupId>
				<artifactId>maven-bundle-plugin</artifactId>
				<version>${bundle.plugin.version}</version>
				<extensions>true</extensions>
				<configuration>
					<instructions>
						<Bundle-SymbolicName>${project.artifactId}</Bundle-SymbolicName>
						<Import-Package>
							*
						</Import-Package>
						<Export-Package>
							org.herasaf.xacml.core.*
						</Export-Package>
					</instructions>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>${compiler.plugin.version}</version>
				<configuration>
					<source>${java.compile.version}</source>
					<target>${java.compile.version}</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>${surefire.plugin.version}</version>
				<configuration>
					<skip>false</skip>
					<argLine>-Xms512m -Xmx1024m</argLine>
					<forkCount>1C</forkCount>
					<reuseForks>false</reuseForks>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-release-plugin</artifactId>
				<version>${release.plugin.version}</version>
			</plugin>
		</plugins>
		<pluginManagement>
			<plugins>
				<!--This plugin's configuration is used to store Eclipse m2e settings only. It has no influence on the Maven build itself. -->
				<plugin>
					<groupId>org.eclipse.m2e</groupId>
					<artifactId>lifecycle-mapping</artifactId>
					<version>1.0.0</version>
					<configuration>
						<lifecycleMappingMetadata>
							<pluginExecutions>
								<pluginExecution>
									<pluginExecutionFilter>
										<groupId>
											org.apache.maven.plugins
										</groupId>
										<artifactId>
											maven-dependency-plugin
										</artifactId>
										<versionRange>
											[2.1,)
										</versionRange>
										<goals>
											<goal>
												unpack-dependencies
											</goal>
										</goals>
									</pluginExecutionFilter>
									<action>
										<ignore />
									</action>
								</pluginExecution>
							</pluginExecutions>
						</lifecycleMappingMetadata>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>

	<profiles>
		<profile>
			<id>release</id>
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-javadoc-plugin</artifactId>
						<version>${javadoc.plugin.version}</version>
						<executions>
							<execution>
								<id>attach-javadoc</id>
								<phase>install</phase>
								<goals>
									<goal>jar</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-source-plugin</artifactId>
						<version>${source.plugin.version}</version>
						<executions>
							<execution>
								<id>attach-sources</id>
								<phase>install</phase>
								<goals>
									<goal>jar-no-fork</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
</project>